import re
from abc import ABC, abstractmethod
from enum import Flag, auto
from typing import TypeVar, Match, Union, Callable, NamedTuple, Iterable
from typing import List, Tuple, Optional, Pattern

from .env import Env
from .shell_actions import ShellAction, UpdateEnv, RunPipeChain


class ParsingException(Exception):
    """
    Exception that signifies a syntactic error was encountered while
    parsing a shell command.
    """


class ShellActionParser(ABC):
    """Abstract base class for parsers"""

    @abstractmethod
    def parse(self, line: str) -> ShellAction:
        """
        Parse one line of input into a shell action to be executed.

        :param line one line of shell input
        :raises ParsingException if the input line can not be correctly parsed
        """


class ChunkFlags(Flag):
    NONE = 0
    SHOULD_SPLIT_SPACES = auto()
    SHOULD_SPLIT_PIPES = auto()
    SHOULD_EXPAND = auto()

    # Escaping

    SHOULD_UNESCAPE_SINGLE_QUOTE = auto()
    SHOULD_UNESCAPE_DOUBLE_QUOTE = auto()
    SHOULD_UNESCAPE_DOLLAR = auto()
    SHOULD_UNESCAPE_ALL = \
        SHOULD_UNESCAPE_SINGLE_QUOTE | \
        SHOULD_UNESCAPE_DOUBLE_QUOTE | \
        SHOULD_UNESCAPE_DOLLAR

    # Aliases for convenience

    INSIDE_DOUBLE_QUOTES = \
        SHOULD_EXPAND | \
        SHOULD_UNESCAPE_DOUBLE_QUOTE | \
        SHOULD_UNESCAPE_DOLLAR
    INSIDE_SINGLE_QUOTES = SHOULD_UNESCAPE_SINGLE_QUOTE
    OUTSIDE = \
        SHOULD_EXPAND | \
        SHOULD_SPLIT_PIPES | \
        SHOULD_SPLIT_SPACES | \
        SHOULD_UNESCAPE_ALL

    # Splitting mark, useful for temporary work, should only be set temporarily
    # during one stage of parsing

    MARK = auto()


class FlaggedChunk(NamedTuple):
    str: str
    flags: ChunkFlags


FlaggedString = Iterable[FlaggedChunk]
Regex = Union[Pattern, str]


def tokenize(pattern: Regex, string: str,
             only_unescaped: bool = None) -> Iterable[Tuple[str, bool]]:
    """
    Yields parts of string, marked as `True` when they match the supplied
    `Regex`, and `False` if the part is outside a match.

    :param pattern: Regex to search for in the string
    :param string: string to search in
    :param only_unescaped: only matches that are not preceded by odd number of
                           backslashes are considered
    :return: iterator over the marked parts of string
    """

    if only_unescaped is None:
        only_unescaped = False

    last = 0
    for match in re.finditer(pattern, string):
        if only_unescaped and escaped(string, match.start()):
            continue
        if match.start() > last:
            yield string[last:match.start()], False
        yield string[match.start():match.end()], True
        last = match.end()
    if last < len(string):
        yield string[last:], False


T = TypeVar('T')


def split_list(predicate: Callable[[T], bool],
               list_: Iterable[T], keep_empty: bool) -> Iterable[List[T]]:
    """
    Analog of str.split() for lists (and generally iterators).

    :param predicate: function that decides if the element is a divider
    :param list_: iterator over the objects, which represents the list to split
    :param keep_empty: if `False`, discard all empty sub lists
    :return: iterator over sub lists after splitting
    """

    leftovers: List[T] = []
    for elem in list_:
        if predicate(elem):
            if keep_empty or len(leftovers) > 0:
                yield leftovers
                leftovers = []
        else:
            leftovers.append(elem)
    if keep_empty or len(leftovers) > 0:
        yield leftovers


def mark_chunk(flagged_chunk: FlaggedChunk,
               on_flag: ChunkFlags, regex: Regex) -> FlaggedString:
    """
    Chops the flagged chunk into parts. Only chops the chunk if it has
    `on_flag` set, in which case also removes the flag. Chopping is done by
    tokenizing based on `regex`, the chunks that are marked during the process
    are given an additional `Flags.MARK` flag, unmarked ones are passed with
    their flags intact.
    """

    chunk, flags = flagged_chunk
    if not flags & on_flag:
        yield flagged_chunk
        return
    flags &= ~on_flag
    for subchunk, marked in tokenize(regex, chunk):
        yield FlaggedChunk(subchunk,
                           flags | ChunkFlags.MARK if marked else flags)


def split_flagged_string(flagged_string: FlaggedString,
                         on_flag: ChunkFlags,
                         regex: Regex,
                         keep_empty: bool = True) -> Iterable[FlaggedString]:
    """
    Mark every chunk by `mark_chunk` and then split the list based on marked
    chunks. `keep_empty` is passed through to `split_list`.
    """

    marked = map(lambda chunk: mark_chunk(chunk, on_flag, regex),
                 flagged_string)
    return split_list(lambda fs: bool(fs.flags & ChunkFlags.MARK),
                      concat(marked), keep_empty)


def concat(iterators: Iterable[Iterable[T]]) -> Iterable[T]:
    """
    Merges multiple iterators into one.
    """

    return [item for iterator in iterators for item in iterator]


def match_start(match: Optional[Match]) -> Optional[int]:
    """
    Converts optional match to an optional position of it's start.
    """

    start = None
    if match is not None:
        start = match.start()
    return start


def escaped(line: str, position: int) -> bool:
    """Check that the given position in string is escaped."""

    current = position - 1
    while current >= 0 and line[current] == '\\':
        current -= 1
    return (position - current) % 2 == 0


def first_unescaped_match(regex: Regex, line: str,
                          start: int) -> Optional[int]:
    """
    Find position of first match of `regex` in string that is not escaped.
    See `escaped()`.
    """

    while True:
        current = match_start(re.compile(regex).search(line, start))
        if current is None:
            return None
        if not escaped(line, current):
            return current
        start = current + 1


class CashV1ActionParser(ShellActionParser):
    VARIABLE_REGEX = re.compile(r'\$\w+')

    def __init__(self, env: Env):
        self.env = env

    @classmethod
    def _process_quotes(cls, line: str) -> FlaggedString:
        current = 0
        while True:
            start = first_unescaped_match("['\"]", line, current)
            if start is None:
                break
            quote = line[start]
            if current < start:
                yield FlaggedChunk(line[current:start], ChunkFlags.OUTSIDE)
            end = first_unescaped_match(quote, line, start + 1)
            if end is None:
                raise ParsingException(f'Unmatched {quote} at {start + 1}')
            yield FlaggedChunk(line[start + 1:end],
                               ChunkFlags.INSIDE_DOUBLE_QUOTES if quote == '"'
                               else ChunkFlags.INSIDE_SINGLE_QUOTES)
            current = end + 1
        if current < len(line):
            yield FlaggedChunk(line[current:], ChunkFlags.OUTSIDE)

    @classmethod
    def _expand(cls, flagged_string: FlaggedString, env: Env) -> FlaggedString:
        for chunk, flags in flagged_string:
            if flags & ChunkFlags.SHOULD_EXPAND:
                flags &= ~ChunkFlags.SHOULD_EXPAND
                for subchunk, is_variable in tokenize(cls.VARIABLE_REGEX,
                                                      chunk,
                                                      only_unescaped=True):
                    yield (FlaggedChunk(env.get(subchunk[1:], ''),
                                        flags
                                        & ~ChunkFlags.SHOULD_SPLIT_PIPES
                                        & ~ChunkFlags.SHOULD_UNESCAPE_ALL)
                           if is_variable else FlaggedChunk(subchunk, flags))
            else:
                yield FlaggedChunk(chunk, flags)

    @staticmethod
    def _unescape(flagged_string: FlaggedString) -> FlaggedString:
        char_to_flag = {"'": ChunkFlags.SHOULD_UNESCAPE_SINGLE_QUOTE,
                        '"': ChunkFlags.SHOULD_UNESCAPE_DOUBLE_QUOTE,
                        '$': ChunkFlags.SHOULD_UNESCAPE_DOLLAR}
        for chunk, flags in flagged_string:
            to_unescape = ''.join(filter(lambda c: flags & char_to_flag[c],
                                         '\'"$'))
            flags &= ~ChunkFlags.SHOULD_UNESCAPE_ALL
            if len(to_unescape) == 0:
                yield FlaggedChunk(chunk, flags)
                continue
            chunk = re.sub(r'\\\\', r'\\', chunk)  # first unescape backslashes
            chunk = re.sub(r'\\([' + to_unescape + r'])', r'\1', chunk)
            yield FlaggedChunk(chunk, flags)

    @staticmethod
    def _split_on_pipes(flagged_string: FlaggedString) \
            -> Iterable[FlaggedString]:
        return split_flagged_string(flagged_string,
                                    ChunkFlags.SHOULD_SPLIT_PIPES, r'\|')

    @staticmethod
    def _split_on_spaces(flagged_string: FlaggedString) \
            -> Iterable[FlaggedString]:
        return split_flagged_string(flagged_string,
                                    ChunkFlags.SHOULD_SPLIT_SPACES, r'\s+',
                                    keep_empty=False)

    @staticmethod
    def _collapse(flagged_strings: Iterable[FlaggedString]) -> List[str]:
        """
        Remove all flags and concatenate chunks that belong to one argument.
        """
        return list(map(lambda x: ''.join(map(lambda y: y.str, x)),
                        flagged_strings))

    def parse(self, line: str) -> ShellAction:
        current_flagged_string = self._process_quotes(line)
        current_flagged_string = self._expand(current_flagged_string, self.env)
        current_flagged_string = self._unescape(current_flagged_string)
        pipes = self._split_on_pipes(current_flagged_string)
        after_spaces_split = map(self._split_on_spaces, pipes)
        collapsed = map(self._collapse, after_spaces_split)
        pipe_chain = list(collapsed)
        if len(pipe_chain) == 1 and len(pipe_chain[0]) == 1 \
                and '=' in pipe_chain[0][0]:
            variable, value = pipe_chain[0][0].split('=', 1)
            return UpdateEnv(variable, value)
        assert len(pipe_chain) > 0
        if len(pipe_chain) == 1 and len(pipe_chain[0]) == 0:
            # we parsed it as one empty command but we should treat this
            # situation as if there were no commands
            pipe_chain = []
        for command in pipe_chain:
            if len(command) == 0:
                raise ParsingException('Empty commands in pipe chain are not '
                                       'allowed')
        return RunPipeChain(pipe_chain)
