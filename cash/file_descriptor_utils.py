import os
import sys
from dataclasses import dataclass
from typing import Callable

FileDescriptor = int


@dataclass
class StandardDescriptors:
    stdin: FileDescriptor
    stdout: FileDescriptor
    stderr: FileDescriptor


def wrap_file_descriptors(func: Callable[[StandardDescriptors], None],
                          input_bytes: bytes) -> bytes:
    """
    Utility function to conveniently pass and get bytes from functions that
    expect input and output to be done trough file descriptors.

    :param func: the function that expects 3 file descriptors: stdin, stdout
                 and stderr
    :param input_bytes: what to pass to the function's input file descriptor

    :return: data from the function's output and error file descriptors
    """

    input_from_pipe, input_to_pipe = os.pipe()
    with open(input_to_pipe, 'wb') as input_stream:
        input_stream.write(input_bytes)

    output_from_pipe, output_to_pipe = os.pipe()

    func(StandardDescriptors(stdin=input_from_pipe,
                             stdout=output_to_pipe,
                             stderr=output_to_pipe))

    with open(output_from_pipe, 'rb') as output_stream:
        output = output_stream.read()

    return output


_PROTECTED_FDS = [sys.stdin.fileno(), sys.stdout.fileno(), sys.stderr.fileno()]


def close_unimportant(file_descriptor: FileDescriptor) -> None:
    """
    Close the file descriptor but only if it's not stdin, stdout or stderr.
    """
    if file_descriptor not in _PROTECTED_FDS:
        os.close(file_descriptor)
