import os
from typing import Type, Optional

from .env import Env
from .command_runners.base import ShellShouldExit
from .parser import CashV1ActionParser, ParsingException, ShellActionParser
from .shell_actions import ShellActionRuntimeException


def print_exception(exception: Exception):
    current_exception: Optional[BaseException] = exception
    while current_exception is not None:
        cause = current_exception.__cause__
        print(str(current_exception) + (':' if cause is not None else ''))
        current_exception = cause


class Cash:
    def __init__(self, prompt: str = None, env: Env = None,
                 parser_type: Type[ShellActionParser] = None):
        """
        Create a new shell instance.

        :param prompt: shell prompt, '> ' by default
        :param env: environment to run shell in, use system environment by
                    default
        :param parser_type: type to use to parse commands, `CashV1ActionParser`
                            by default
        """
        if prompt is None:
            prompt = "> "
        if env is None:
            env = os.environ
        if parser_type is None:
            parser_type = CashV1ActionParser
        self.prompt = prompt
        self.env = env
        self.parser_type = parser_type

    def loop(self):
        parser = self.parser_type(self.env)
        while True:
            try:
                line = input(self.prompt)
            except EOFError:
                print()
                break
            try:
                parser.parse(line).run(self.env)
            except ParsingException as exception:
                print('Exception while parsing the previous command:')
                print_exception(exception)
            except ShellActionRuntimeException as exception:
                print('Exception while running the previous command:')
                print_exception(exception)
            except ShellShouldExit:
                break
