from abc import ABC, abstractmethod
from typing import List

from cash.file_descriptor_utils import StandardDescriptors
from ..env import Env

Command = List[str]


class ShellShouldExit(Exception):
    """Exception that signifies that the current shell should exit normally."""


class CommandRunner(ABC):
    """Abstract class for command runners."""

    @abstractmethod
    def run(self, command: Command, env: Env,
            fds: StandardDescriptors) -> None:
        """
        Run the command. `stdin` and `stdout` should be closed using
        `close_unimportant()`.
        """
