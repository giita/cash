from argparse import Namespace, ArgumentParser

from ..argparse_utils import ArgParseCommandRunner
from ...env import Env
from ...file_descriptor_utils import StandardDescriptors


class CatBuiltin(ArgParseCommandRunner):
    def run_args(self, args: Namespace, env: Env,
                 fds: StandardDescriptors) -> None:
        with open(fds.stdin, 'rb', closefd=False) as input_stream, \
             open(fds.stdout, 'wb', closefd=False) as output_stream:
            if len(args.files) == 0:
                for line in input_stream:
                    output_stream.write(line)
                return
            for filename in args.files:
                with open(filename, 'rb') as content_file:
                    for line in content_file:
                        output_stream.write(line)

    @staticmethod
    def _customize_argument_parser(parser) -> ArgumentParser:
        parser.description = 'Concatenate contents of files passed as ' \
                             'arguments to stdout. If no arguments ' \
                             'are supplied copy stdin to stdout.'
        parser.add_argument('files', nargs='*')
        return parser
