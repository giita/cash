from argparse import ArgumentParser, Namespace

from ..argparse_utils import ArgParseCommandRunner
from ..base import ShellShouldExit
from ...env import Env
from ...file_descriptor_utils import StandardDescriptors


class ExitBuiltin(ArgParseCommandRunner):
    def run_args(self, args: Namespace, env: Env,
                 fds: StandardDescriptors) -> None:
        raise ShellShouldExit()

    @staticmethod
    def _customize_argument_parser(parser: ArgumentParser) -> ArgumentParser:
        parser.description = 'Exit from the shell.'
        return parser
