import re
from argparse import Namespace, ArgumentParser, ArgumentTypeError

from ..argparse_utils import ArgParseCommandRunner
from ...env import Env
from ...file_descriptor_utils import StandardDescriptors


def non_negative(param: str) -> int:
    i = int(param)
    if i < 0:
        raise ArgumentTypeError(f'{i} is an invalid non-negative int value')
    return i


class GrepBuiltin(ArgParseCommandRunner):
    def run_args(self, args: Namespace, env: Env,
                 fds: StandardDescriptors) -> None:
        with (open(fds.stdin, 'r', closefd=False) if args.file is None
              else open(args.file, 'r')) as input_stream, \
             open(fds.stdout, 'w', closefd=False) as output_stream:
            momentum = 0
            flags = re.IGNORECASE if args.ignore_case else 0
            pattern = args.pattern
            if args.word_regexp:
                pattern = r'((?<=\W)|^)' + pattern + r'((?=\W)|$)'
            for line in input_stream:
                line = line.rstrip('\n')
                if re.search(pattern, line, flags) is not None:
                    momentum = args.after + 1
                if momentum > 0:
                    print(line, file=output_stream)
                    momentum -= 1

    @staticmethod
    def _customize_argument_parser(parser: ArgumentParser) -> ArgumentParser:
        parser.description = 'Search a given file (or standard input) ' \
                             'for a regular expression.'
        parser.add_argument('pattern')
        parser.add_argument('file', nargs='?')
        parser.add_argument('-A', dest='after', type=non_negative, default=0,
                            help='how many additional lines to print after '
                                 'each match, should be non-negative')
        parser.add_argument('-i', '--ignore-case', action='store_true',
                            help='ignore case while matching')
        # this meaning of -w matches GNU grep
        parser.add_argument('-w', '--word-regexp', action='store_true',
                            help='match whole words only, which means '
                            'that the matches must be preceded by start of '
                            'line or a non-word constituent character, and '
                            'followed by end of line or a non-word '
                            'constituent character')
        return parser
