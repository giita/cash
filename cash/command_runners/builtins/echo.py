from argparse import Namespace, ArgumentParser

from ..argparse_utils import ArgParseCommandRunner
from ...env import Env
from ...file_descriptor_utils import StandardDescriptors


class EchoBuiltin(ArgParseCommandRunner):
    def run_args(self, args: Namespace, env: Env,
                 fds: StandardDescriptors) -> None:
        with open(fds.stdout, 'w', closefd=False) as output_stream:
            print(' '.join(args.strings), file=output_stream)

    @staticmethod
    def _customize_argument_parser(parser: ArgumentParser) -> ArgumentParser:
        parser.description = 'Print arguments to stdout separated by space.'
        parser.add_argument('strings', nargs='*')
        return parser
