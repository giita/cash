from argparse import Namespace, ArgumentParser
from dataclasses import dataclass
from typing import Iterable

from ..argparse_utils import ArgParseCommandRunner
from ...env import Env
from ...file_descriptor_utils import StandardDescriptors


@dataclass
class WcStats:
    lines: int
    words: int
    bytes: int


class WcBuiltin(ArgParseCommandRunner):
    @staticmethod
    def get_stats(lines: Iterable[bytes]) -> WcStats:
        stats = WcStats(0, 0, 0)
        for line in lines:
            stats.lines += 1
            stats.words += len(line.split())
            stats.bytes += len(line)
        return stats

    @staticmethod
    def format_stats(stats: WcStats, filename: str = None) -> str:
        result = f'{stats.lines} {stats.words} {stats.bytes}'
        if filename is not None:
            result += ' ' + filename
        return result

    def run_args(self, args: Namespace, env: Env,
                 fds: StandardDescriptors) -> None:
        with open(fds.stdin, 'rb', closefd=False) as input_stream, \
             open(fds.stdout, 'w', closefd=False) as output_stream:
            if len(args.files) == 0:
                print(self.format_stats(
                    self.get_stats(input_stream.readlines())
                ), file=output_stream)
                return
            total = WcStats(0, 0, 0)
            for filename in args.files:
                with open(filename, 'rb') as content_file:
                    stats = self.get_stats(content_file)
                    print(self.format_stats(stats, filename),
                          file=output_stream)
                    total.lines += stats.lines
                    total.words += stats.words
                    total.bytes += stats.bytes
            if len(args.files) > 1:
                print(self.format_stats(total, 'total'), file=output_stream)

    @staticmethod
    def _customize_argument_parser(parser: ArgumentParser) -> ArgumentParser:
        parser.description = 'Count lines, words and bytes in the files. ' \
                             'If no files are supplied inspect stdin instead.'
        parser.add_argument('files', nargs='*')
        return parser
