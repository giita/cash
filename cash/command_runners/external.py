import subprocess

from .base import CommandRunner, Command
from ..env import Env
from ..file_descriptor_utils import close_unimportant, StandardDescriptors


class ExternalCommandRunner(CommandRunner):
    """Runs a new process and waits for it to finish."""

    def run(self, command: Command, env: Env,
            fds: StandardDescriptors) -> None:
        result = subprocess.Popen(command, env=env,
                                  stdin=fds.stdin,
                                  stdout=fds.stdout,
                                  stderr=fds.stderr)
        close_unimportant(fds.stdin)
        close_unimportant(fds.stdout)
        result.wait()
