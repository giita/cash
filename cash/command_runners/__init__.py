from typing import Type

from .base import CommandRunner
from .builtins.cat import CatBuiltin
from .builtins.echo import EchoBuiltin
from .builtins.exit import ExitBuiltin
from .builtins.pwd import PwdBuiltin
from .builtins.wc import WcBuiltin
from .builtins.grep import GrepBuiltin
from .external import ExternalCommandRunner

_BUILTINS = {
    'cat': CatBuiltin,
    'echo': EchoBuiltin,
    'wc': WcBuiltin,
    'pwd': PwdBuiltin,
    'exit': ExitBuiltin,
    'grep': GrepBuiltin
}


def runner_for(command_name: str) -> Type[CommandRunner]:
    return _BUILTINS.get(command_name, ExternalCommandRunner)
