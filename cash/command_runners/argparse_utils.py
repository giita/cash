import sys
from abc import abstractmethod
from argparse import ArgumentParser, Namespace
from gettext import gettext as _
from typing import IO

from .base import CommandRunner, Command
from ..env import Env
from ..file_descriptor_utils import close_unimportant, StandardDescriptors


class ArgumentParserExited(Exception):
    """
    Exception that signifies that an argument parser decided that
    current program should exit.
    """


class NoExitArgumentParser(ArgumentParser):
    """
    By default ArgumentParser exits when a help is shown or an error occurs.
    This subclass throws an exception instead, which can be caught at the
    time `parse_args` is called.
    """

    def exit(self, status=0, message=None):
        if message:
            self._print_message(message, sys.stderr)
        raise ArgumentParserExited()

    def error(self, message):
        """
        Prints a usage message incorporating the message to stderr and
        throws an exception signaling that the parser exited.
        """

        self.print_usage(sys.stderr)
        args = {'prog': self.prog, 'message': message}
        self._print_message(_('%(prog)s: error: %(message)s\n') % args,
                            sys.stderr)
        raise ArgumentParserExited()


class RedirectStdStreams:
    """
    Temporarily redirect standard output streams. Should be used with
    `with` construct.
    """

    old_stdout: IO
    old_stderr: IO

    def __init__(self, stdout: IO = None, stderr: IO = None):
        self.new_stdout = stdout or sys.stdout
        self.new_stderr = stderr or sys.stderr

    def __enter__(self):
        self.old_stdout, self.old_stderr = sys.stdout, sys.stderr
        self.old_stdout.flush()
        self.old_stderr.flush()
        sys.stdout, sys.stderr = self.new_stdout, self.new_stderr

    def __exit__(self, exc_type, exc_value, traceback):
        self.new_stdout.flush()
        self.new_stderr.flush()
        sys.stdout = self.old_stdout
        sys.stderr = self.old_stderr


class ArgParseCommandRunner(CommandRunner):
    """Base class for builtins that use the `argparse` library."""

    def run(self, command: Command, env: Env,
            fds: StandardDescriptors) -> None:
        base_parser = NoExitArgumentParser(command[0])
        custom_parser = self._customize_argument_parser(base_parser)
        try:
            with open(fds.stdout, 'w', closefd=False) as tmp_stdout, \
                 open(fds.stderr, 'w', closefd=False) as tmp_stderr, \
                 RedirectStdStreams(tmp_stdout, tmp_stderr):
                # we need to do this because in many cases ArgumentParser
                # uses standard streams to output information, but we have
                # our own
                args = custom_parser.parse_args(command[1:])
            self.run_args(args, env, fds)
        except ArgumentParserExited:
            pass
        close_unimportant(fds.stdin)
        close_unimportant(fds.stdout)

    @abstractmethod
    def run_args(self, args: Namespace, env: Env,
                 fds: StandardDescriptors) -> None:
        """
        Implementations should override this instead of `run()`. We let
        subclasses open files by themselves, but they SHOULD NOT close the
        file descriptor, for example by passing `closefd=False` to `open()`.
        """

    @staticmethod
    def _customize_argument_parser(parser: ArgumentParser) -> ArgumentParser:
        """
        Implementations can override this static method to provide
        customized options.
        """
        return parser
