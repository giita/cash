import os
import sys
from abc import ABC, abstractmethod
from typing import Iterable

from .file_descriptor_utils import StandardDescriptors
from .command_runners import runner_for
from .command_runners.base import Command, ShellShouldExit
from .env import Env


class ShellActionRuntimeException(Exception):
    """
    Exception that signifies that an exception occurred during execution
    of a shell action.
    """


class ShellAction(ABC):
    """A general action shell can perform."""

    @abstractmethod
    def run(self, env: Env) -> None:
        """Execute the action in given environment."""


class UpdateEnv(ShellAction):
    """Action of updating shell's environment."""

    def __init__(self, variable: str, value: str) -> None:
        self.variable = variable
        self.value = value

    def run(self, env: Env) -> None:
        env[self.variable] = self.value


class RunPipeChain(ShellAction):
    """Action of running a piped command chain."""

    def __init__(self, pipe_chain: Iterable[Command]) -> None:
        self.pipe_chain = pipe_chain

    def run(self, env: Env, fds: StandardDescriptors = None) -> None:
        if fds is None:
            fds = StandardDescriptors(stdin=sys.stdin.fileno(),
                                      stdout=sys.stdout.fileno(),
                                      stderr=sys.stderr.fileno())

        pipe_chain = list(self.pipe_chain)
        cur_from = fds.stdin
        for i, command in enumerate(pipe_chain):
            runner = runner_for(command[0])()

            is_last = i == len(pipe_chain) - 1
            if is_last:
                next_from, cur_to = None, fds.stdout
            else:
                next_from, cur_to = os.pipe()

            try:
                runner.run(command, env,
                           StandardDescriptors(stdin=cur_from,
                                               stdout=cur_to,
                                               stderr=fds.stderr))
            except ShellShouldExit as exception:
                raise exception
            except Exception as exception:
                raise ShellActionRuntimeException(
                    'Error while running pipe chain'
                ) from exception

            if next_from is not None:  # to please the type checker
                cur_from = next_from
