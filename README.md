This is a simple shell implemented in Python.

### Requirements

A recent version of Python. Tested on 3.8.1, but 3.7 should work too.

### How to run

`./cash.py`

if your system understands shebangs, in other case use an analog of

`python cash.py`

### How to test

Run

`python -m unittest -v`

### How to type-check

You can use `mypy`. Run

`mypy .`

### Main features

  * Supports escapes
  * Can interactively run external programs
  * Commands can take input from console until EOF
  * All builtins use argparse library for parsing command line arguments
  * Extensive use of Python typing hints

### Implementation details

  * Commands use file descriptors as abstraction of input/output/error streams, because that's what external commands use. Just passing stdin and stdout to external command allows them to run interactively. `os.pipe()` is used for communication between commands.
  * Parser uses an abstraction of a string, some parts of which are marked by flags. This allows fine-grained control over what happens with which parts of string. Behavior of escaping, quoting end expanding is based on bash and tries to replicate it as much as possible.
  * `os.environ` is used as an environment, but each part of code can use any `dict()` in place of it. It is useful in testing.
  * Exit functionality is implemented as a custom exception.
  * Commands are run one by one and not in parallel to simplify implementation.

### Architecture
![Architecture](cash-architecture.png)
