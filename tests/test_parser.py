from typing import List
from unittest import TestCase

from cash.command_runners.base import Command
from cash.parser import CashV1ActionParser, ParsingException
from cash.shell_actions import RunPipeChain, UpdateEnv


class ParserTest(TestCase):
    def assertRunPipeChainEqual(self, a: RunPipeChain, b: RunPipeChain,
                                msg: str) -> None:
        if msg is None:
            msg = 'Pipe chains not equal'
        self.assertEqual(a.pipe_chain, b.pipe_chain, msg)

    def assertUpdateEnvEqual(self, a: UpdateEnv, b: UpdateEnv,
                             msg: str) -> None:
        self.assertEqual(a.variable, b.variable)
        self.assertEqual(a.value, b.value)

    def setUp(self) -> None:
        self.parser = CashV1ActionParser({'simple': 'easy',
                                          'spaces': ' 2  3 ',
                                          'pipe': 'a | b',
                                          'escapes': '\\\'\\"\\$\\\\'})
        self.addTypeEqualityFunc(RunPipeChain, self.assertRunPipeChainEqual)
        self.addTypeEqualityFunc(UpdateEnv, self.assertUpdateEnvEqual)

    def correctly_parsed(self, expected: List[Command], line: str) -> None:
        self.assertEqual(RunPipeChain(expected), self.parser.parse(line))

    def test_smoke(self) -> None:
        self.correctly_parsed([['echo', '123'], ['wc']], 'echo 123 | wc')

    # Double quotes

    def test_double_quotes_basic(self) -> None:
        self.correctly_parsed([['echo', '123']], 'echo "123"')

    def test_double_quotes_raises_on_unmatched(self) -> None:
        self.assertRaisesRegex(ParsingException, 'Unmatched " at 6',
                               lambda: self.parser.parse('echo "123'))

    def test_double_quotes_should_substitute(self) -> None:
        self.correctly_parsed([['echo', '123easy 45']], 'echo "123$simple 45"')

    # Single quotes

    def test_single_quotes_basic(self) -> None:
        self.correctly_parsed([['echo', '123']], "echo '123'")

    def test_single_quotes_raises_on_unmatched(self) -> None:
        self.assertRaisesRegex(ParsingException, "Unmatched ' at 6",
                               lambda: self.parser.parse("echo '123"))

    def test_single_quotes_should_not_substitute(self) -> None:
        self.correctly_parsed([['echo', '123$simple 456']],
                              "echo '123$simple 456'")

    # Pipes

    def test_pipes_basic(self) -> None:
        self.correctly_parsed([['echo', '123'], ['wc']], 'echo "123" | wc')

    def test_pipe_in_single_quotes_is_literal(self) -> None:
        self.correctly_parsed([['echo', '123', '| wc']], "echo 123 '| wc'")

    def test_pipe_in_double_quotes_is_literal(self) -> None:
        self.correctly_parsed([['echo', '123', '| wc']], 'echo 123 "| wc"')

    def test_pipes_empty(self) -> None:
        self.correctly_parsed([], '')

    def test_pipes_spaces(self) -> None:
        self.correctly_parsed([], '    ')

    def test_just_pipe_raises(self) -> None:
        self.assertRaisesRegex(ParsingException,
                               "Empty commands in pipe chain are not allowed",
                               lambda: self.parser.parse('|'))

    def test_pipe_with_spaces_raises(self) -> None:
        self.assertRaisesRegex(ParsingException,
                               "Empty commands in pipe chain are not allowed",
                               lambda: self.parser.parse(' |  '))

    def test_empty_before_pipe_raises(self) -> None:
        self.assertRaisesRegex(ParsingException,
                               "Empty commands in pipe chain are not allowed",
                               lambda: self.parser.parse('| cat'))

    def test_spaces_before_pipe_raises(self) -> None:
        self.assertRaisesRegex(ParsingException,
                               "Empty commands in pipe chain are not allowed",
                               lambda: self.parser.parse('   | cat'))

    def test_empty_after_pipe_raises(self) -> None:
        self.assertRaisesRegex(ParsingException,
                               "Empty commands in pipe chain are not allowed",
                               lambda: self.parser.parse('echo 123 |'))

    def test_spaces_after_pipe_raises(self) -> None:
        self.assertRaisesRegex(ParsingException,
                               "Empty commands in pipe chain are not allowed",
                               lambda: self.parser.parse('echo 123 | '))

    def test_two_consecutive_pipes_raises(self) -> None:
        self.assertRaisesRegex(ParsingException,
                               "Empty commands in pipe chain are not allowed",
                               lambda: self.parser.parse('echo 123 || cat'))

    def test_spaces_between_pipes_raises(self) -> None:
        self.assertRaisesRegex(ParsingException,
                               "Empty commands in pipe chain are not allowed",
                               lambda: self.parser.parse('echo 123 |  | cat'))

    # Escaping

    def test_quotes_unescaped_outside_quotes(self) -> None:
        self.correctly_parsed([['echo', '\'\"']], 'echo \\\'\\\"')

    def test_dollar_unescaped_outside_quotes(self) -> None:
        self.correctly_parsed([['echo', '$ab$']], 'echo \\$ab\\$')

    def test_backslash_unescaped_outside_quotes(self) -> None:
        self.correctly_parsed([['echo', '\\']], 'echo \\\\')

    def test_single_quotes_unescaped_inside_single_quotes(self) -> None:
        self.correctly_parsed([['echo', "'"]], r"echo '\''")

    def test_backslash_unescaped_inside_single_quotes(self) -> None:
        self.correctly_parsed([['echo', '\\']], r"echo '\\'")

    def test_double_quotes_not_unescaped_inside_single_quotes(self) -> None:
        self.correctly_parsed([['echo', r'\"']], "echo '\\\"'")

    def test_dollar_not_unescaped_inside_single_quotes(self) -> None:
        self.correctly_parsed([['echo', r'\$']], "echo '\\$'")

    def test_double_quotes_unescaped_inside_double_quotes(self) -> None:
        self.correctly_parsed([['echo', '"']], r'echo "\""')

    def test_dollar_unescaped_inside_double_quotes(self) -> None:
        self.correctly_parsed([['echo', '$']], r'echo "\$"')

    def test_backslash_unescaped_inside_double_quotes(self) -> None:
        self.correctly_parsed([['echo', '\\']], r'echo "\\"')

    def test_single_quotes_not_unescaped_inside_double_quotes(self) -> None:
        self.correctly_parsed([['echo', r"\'"]], r'echo "\'"')

    def test_long_escaping(self) -> None:
        self.correctly_parsed([['echo', r'\\\\']], r"echo '\\\\\\\\'")
        self.correctly_parsed([['echo', r'\\\\']], r"echo \\\\\\\\")
        self.assertRaisesRegex(ParsingException, "Unmatched ' at 6",
                               lambda: self.parser.parse(r"echo '\\\\\\\\\'"))
        self.correctly_parsed([['echo', r'\\\\']], r"echo \\\\\\\ ")

    # Expanding

    def test_can_expand_outside_quotes(self) -> None:
        self.correctly_parsed([['echo', 'easy']], 'echo $simple')

    def test_can_expand_in_double_quotes(self) -> None:
        self.correctly_parsed([['echo', 'easy']], 'echo "$simple"')

    def test_can_not_expand_in_single_quotes(self) -> None:
        self.correctly_parsed([['echo', '$simple']], "echo '$simple'")

    def test_expanded_is_splitted_by_spaces_outside_quotes(self) -> None:
        self.correctly_parsed([['echo', '2', '3']], "echo $spaces")

    def test_expanded_is_not_splitted_by_spaces_in_double_quotes(self) -> None:
        self.correctly_parsed([['echo', ' 2  3 ']], 'echo "$spaces"')

    def test_expanded_is_not_splitted_by_pipes_outside_quotes(self) -> None:
        self.correctly_parsed([['echo', 'a', '|', 'b']], "echo $pipe")

    def test_expanded_is_not_splitted_by_pipes_in_double_quotes(self) -> None:
        self.correctly_parsed([['echo', 'a | b']], 'echo "$pipe"')

    def test_no_unescape_in_expanded_outside_quotes(self) -> None:
        self.correctly_parsed([['echo', '\\\'\\"\\$\\\\']], 'echo $escapes')

    def test_no_unescape_in_expanded_in_double_quotes(self) -> None:
        self.correctly_parsed([['echo', '\\\'\\"\\$\\\\']], 'echo "$escapes"')

    # Variable assignment

    def test_assignment(self) -> None:
        self.assertEqual(UpdateEnv('abc', 'def'), self.parser.parse('abc=def'))

    def test_equals_sign_but_no_assignment_because_space(self) -> None:
        self.correctly_parsed([['abc=def', 'argument']], 'abc=def argument')

    def test_equals_sign_but_no_assignment_because_pipe(self) -> None:
        self.correctly_parsed([['abc=def'], ['other']], 'abc=def|other')
