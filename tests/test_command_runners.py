import os
from abc import abstractmethod
from unittest import TestCase

from cash.command_runners import ExternalCommandRunner, CatBuiltin, \
    EchoBuiltin, WcBuiltin, PwdBuiltin, ExitBuiltin, GrepBuiltin
from cash.command_runners.base import Command, ShellShouldExit, CommandRunner
from cash.file_descriptor_utils import wrap_file_descriptors
from .util import EOL


class CommandTestCase(TestCase):
    @abstractmethod
    def get_command_runner(self) -> CommandRunner:
        pass

    def run_command(self, command: Command,
                    input_bytes: bytes = None) -> bytes:
        if input_bytes is None:
            input_bytes = b''
        # need to pass os.environ because on Windows it contains important
        # things
        runner = self.get_command_runner()
        return wrap_file_descriptors(lambda fds:
                                     runner.run(command, os.environ, fds),
                                     input_bytes)


class ExternalCommandTest(CommandTestCase):
    def get_command_runner(self) -> CommandRunner:
        return ExternalCommandRunner()

    def test_git(self) -> None:
        self.assertRegex(self.run_command(['git', '--version'], b'abab'),
                         br'git version .*')


class CatTest(CommandTestCase):
    def get_command_runner(self) -> CommandRunner:
        return CatBuiltin()

    def test_empty_parameters(self) -> None:
        self.assertEqual(b'abab', self.run_command(['cat'], b'abab'))

    def test_one_argument(self) -> None:
        self.assertEqual(b'123\n456 78\n',
                         self.run_command(['cat', 'tests/filetest'], b'abab'))

    def test_two_arguments(self) -> None:
        self.assertEqual(b'123\n456 78\n123\n456 78\n',
                         self.run_command(['cat',
                                           'tests/filetest',
                                           'tests/filetest'],
                                          b'abab'))


class EchoTest(CommandTestCase):
    def get_command_runner(self) -> CommandRunner:
        return EchoBuiltin()

    def test_empty_parameters(self) -> None:
        self.assertEqual(EOL, self.run_command(['echo'], b'abab'))

    def test_one_argument(self) -> None:
        self.assertEqual(b'12  3' + EOL,
                         self.run_command(['echo', '12  3'], b'abab'))

    def test_two_arguments(self) -> None:
        self.assertEqual(b'12  3   4  ' + EOL,
                         self.run_command(['echo', '12  3', '  4  '], b'abab'))


class WcTest(CommandTestCase):
    def get_command_runner(self) -> CommandRunner:
        return WcBuiltin()

    def test_empty_parameters(self) -> None:
        self.assertEqual(b'2 3 12' + EOL,
                         self.run_command(['wc'], b'abab\nab ab \n'))

    def test_one_argument(self) -> None:
        self.assertEqual(b'2 3 11 tests/filetest' + EOL,
                         self.run_command(['wc', 'tests/filetest'], b'abab'))

    def test_two_arguments(self) -> None:
        self.assertEqual(b'2 3 11 tests/filetest' + EOL +
                         b'2 3 11 tests/filetest' + EOL +
                         b'4 6 22 total' + EOL,
                         self.run_command(['wc',
                                           'tests/filetest',
                                           'tests/filetest'],
                                          b'abab'))


class PwdTest(CommandTestCase):
    def get_command_runner(self) -> CommandRunner:
        return PwdBuiltin()

    def test_empty_parameters(self) -> None:
        self.assertEqual(os.getcwdb() + EOL,
                         self.run_command(['pwd'], b'abab'))

    def test_one_argument(self) -> None:
        self.assertEqual(b'usage: pwd [-h]' + EOL +
                         b'pwd: error: unrecognized arguments: 123' + EOL,
                         self.run_command(['pwd', '123'], b'abab'))


class ExitTest(CommandTestCase):
    def get_command_runner(self) -> CommandRunner:
        return ExitBuiltin()

    def test_empty_parameters(self) -> None:
        self.assertRaises(ShellShouldExit,
                          lambda: self.run_command(['exit'], b'abab'))

    def test_one_argument(self) -> None:
        self.assertEqual(b'usage: exit [-h]' + EOL +
                         b'exit: error: unrecognized arguments: 123' + EOL,
                         self.run_command(['exit', '123'], b'abab'))


class GrepTest(CommandTestCase):
    def get_command_runner(self) -> CommandRunner:
        return GrepBuiltin()

    def test_basic(self) -> None:
        self.assertEqual(b'aaa' + EOL,
                         self.run_command(['grep', 'a'],
                                          b'aaa' + EOL + b'bbb' + EOL))

    def test_file_parameter(self) -> None:
        self.assertEqual(b'123' + EOL,
                         self.run_command(['grep', '23', 'tests/filetest'],
                                          b'abab'))

    def test_case_insensitive(self) -> None:
        self.assertEqual(b'aaaa' + EOL +
                         b'AAAA' + EOL,
                         self.run_command(['grep', '-i', 'a'],
                                          b'aaaa' + EOL +
                                          b'bbbb' + EOL +
                                          b'AAAA' + EOL))

    def test_word_regexp(self) -> None:
        self.assertEqual(b'ab' + EOL +
                         b' ab' + EOL +
                         b' ab ' + EOL +
                         b' ab.cc ' + EOL +
                         b" cc'ab " + EOL +
                         b'ab ' + EOL,
                         self.run_command(['grep', '-w', 'ab'],
                                          b'ab' + EOL +
                                          b' aab ' + EOL +
                                          b' abb ' + EOL +
                                          b' ababab ' + EOL +
                                          b' ab' + EOL +
                                          b' ab ' + EOL +
                                          b' ab.cc ' + EOL +
                                          b" cc'ab " + EOL +
                                          b' aba ' + EOL +
                                          b'ab ' + EOL))

    def test_word_regexp_correct_boundary(self) -> None:
        self.assertEqual(b'bab' + EOL +
                         b' bab ' + EOL +
                         b' a ' + EOL +
                         b'b  a  b' + EOL +
                         b' ba ' + EOL,
                         self.run_command(['grep', '-w', '.a.'],
                                          b'bab' + EOL +
                                          b' bab ' + EOL +
                                          b'b  a b' + EOL +
                                          b'b  acb' + EOL +
                                          b'b a b' + EOL +
                                          b' a ' + EOL +
                                          b'b  a  b' + EOL +
                                          b' ba ' + EOL))

    def test_after(self) -> None:
        self.assertEqual(b'a' + EOL +
                         b'b' + EOL +
                         b'bb' + EOL +
                         b'aa' + EOL +
                         b'b' + EOL +
                         b'aaa' + EOL +
                         b'b' + EOL +
                         b'bb' + EOL,
                         self.run_command(['grep', '-A', '2', 'a'],
                                          b'a' + EOL +
                                          b'b' + EOL +
                                          b'bb' + EOL +
                                          b'bbb' + EOL +
                                          b'aa' + EOL +
                                          b'b' + EOL +
                                          b'aaa' + EOL +
                                          b'b' + EOL +
                                          b'bb' + EOL +
                                          b'bbb' + EOL))

    def test_negative_context_error(self) -> None:
        self.assertEqual(b'usage: grep [-h] [-A AFTER] [-i] [-w] '
                         b'pattern [file]' + EOL +
                         b'grep: error: argument -A: -10 is an invalid ' +
                         b'non-negative int value' + EOL,
                         self.run_command(['grep', '-A', '-10', 'ab'],
                                          b'abab'))

    def test_two_files_error(self) -> None:
        self.assertEqual(b'usage: grep [-h] [-A AFTER] [-i] [-w] '
                         b'pattern [file]' + EOL +
                         b'grep: error: unrecognized arguments: file2' + EOL,
                         self.run_command(['grep', 'ab', 'file1', 'file2'],
                                          b'abab'))

    def test_regexp(self) -> None:
        self.assertEqual(b'abcd' + EOL +
                         b'acd' + EOL +
                         b'abbcd' + EOL +
                         b'abccd' + EOL +
                         b'abce' + EOL,
                         self.run_command(['grep', 'ab*c+(d|e)'],
                                          b'abcd' + EOL +
                                          b'acd' + EOL +
                                          b'abbcd' + EOL +
                                          b'abccd' + EOL +
                                          b'abce' + EOL +
                                          b'abd' + EOL +
                                          b'abc' + EOL +
                                          b'bcd' + EOL +
                                          b'abck' + EOL))
