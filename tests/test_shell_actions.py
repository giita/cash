import os
from typing import Iterable
from unittest import TestCase

from cash.command_runners.base import Command
from cash.env import Env
from cash.file_descriptor_utils import wrap_file_descriptors
from cash.shell_actions import UpdateEnv, RunPipeChain
from .util import EOL


class UpdateEnvTest(TestCase):
    def test_new_key(self) -> None:
        env: Env = {}
        UpdateEnv('abab', '123').run(env)
        self.assertEqual({'abab': '123'}, env)

    def test_existing_key(self) -> None:
        env = {'abab': '123'}
        UpdateEnv('abab', '321').run(env)
        self.assertEqual({'abab': '321'}, env)


class RunPipeChainTest(TestCase):
    @staticmethod
    def run_pipe_chain(chain: Iterable[Command],
                       input_bytes: bytes = None) -> bytes:
        if input_bytes is None:
            input_bytes = b''
        # need to pass os.environ because on Windows it contains important
        # things
        return wrap_file_descriptors(lambda fds:
                                     RunPipeChain(chain).run(os.environ, fds),
                                     input_bytes)

    def test_one_builtin(self) -> None:
        self.assertEqual(b'abab', self.run_pipe_chain([['cat']], b'abab'))

    def test_one_external(self) -> None:
        self.assertRegex(self.run_pipe_chain([['git', '--version']], b'abab'),
                         b'git version .*')

    def test_two_builtins(self) -> None:
        self.assertEqual(b'abab',
                         self.run_pipe_chain([['cat'], ['cat']], b'abab'))

    def test_generated_by_first(self) -> None:
        self.assertEqual(b'1 1 %d' % (3 + len(EOL)) + EOL,
                         self.run_pipe_chain([['echo', '123'], ['wc']], b''))

    def test_external_in_pipe_first(self) -> None:
        self.assertRegex(self.run_pipe_chain([['git', '--version'], ['cat']],
                                             b'abab'),
                         b'git version .*')

    def test_external_in_pipe_last(self) -> None:
        self.assertRegex(self.run_pipe_chain([['cat'], ['git', '--version']],
                                             b'abab'),
                         b'git version .*')

    def test_two_external(self) -> None:
        self.assertRegex(self.run_pipe_chain([['git', '--version'],
                                              ['git', '--version']],
                                             b'abab'),
                         b'git version .*')
